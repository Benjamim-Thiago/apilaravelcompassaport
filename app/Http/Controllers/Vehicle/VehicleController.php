<?php

namespace App\Http\Controllers\Vehicle;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Vehicle;
use Validator;
use Illuminate\Pagination\Paginator;
class VehicleController extends Controller
{
    protected function validateVehicle($request){
        $validated = Validator::make($request->all(), [
            'brand' => 'required|string|max:50',
            'model' => 'required|string|max:50',
            'year' => 'required|numeric|min:4',
            'price' => 'required|numeric|min:1'
        ],
        [
            'brand.required' => 'Marca obrigatorio',
            'brand.max' => 'Marca deve conter no máximo 50 caracteres',
            'model.required' => 'Modelo obrigatorio',
            'model.max' => 'Modelo deve conter no máximo 50 caracteres',
            'year.required' => 'Ano obrigatorio',
            'year.numeric' => 'Ano deve ser numerico',
            'price.required' => 'Preço obrigatorio',
            'price.numeric' => 'Preço deve ser numerico'
        ]);

        return $validated;
    }

    public function index(Request $request)
    {
        try {
            $qtd = $request['qtd'];
            $page = $request['page'];

            Paginator::currentPageResolver(function () use ($page) {
                return $page;
            });

            $vehicle = Vehicle::paginate($qtd);

            $vehicle = $vehicle->appends(Request::capture()->except('page')); 

            return response()->json(['veiculos'=>$vehicle], 200);
        } catch(\Exception $ex){
            return response()->json(['message'=>'erro ao se conectar com servidor'], 500);
        }
    }
    
    public function store(Request $request)
    {
        try{
            $validated = $this->validateVehicle($request);
            if($validated->fails()){
                return response()->json(['message'=>'Erro', 
                    'errors' => $validated->errors()], 
                    400);
            }
            
            $data = $request->only(['brand', 'model', 'year', 'price']);
            if($data){
                $vehicle = Vehicle::create($data);
                if($vehicle){
                    return response()->json(['data'=> $vehicle], 201);
                }else{
                    return response()->json(['message'=>'Erro ao criar o veículo'], 400);
                }
            }else{
                return response()->json(['message'=>'Dados inválidos'], 400);
            }
        }catch(\Exception $ex){
            return response()->json(['message'=>'erro ao se conectar com servidor'], 500);
        }    
    }

    public function findId($id)
    {
        try{
            if($id < 0){
                return response()->json(['message'=>'ID menor que zero, por favor, informe um ID válido'], 400);
            }
        
            $vehicle = Vehicle::find($id);
            if(!$vehicle){
                return response()->json(['message'=>'O veiculo com id '.$id.' nao existe'], 404);
            }
            return response()->json([$vehicle], 200);
        
        }catch(\Exception $ex){
            return response()->json(['message'=>'erro ao se conectar com servidor'], 500);
        }
    }

    public function update(Request $request, $id)
    {
        try{
            $validated = $this->validateVehicle($request);
            if($validated->fails()){
                return response()->json(['message'=>'Erro','errors' => $validated->errors()], 400);
            }
            
            $data = $request->only(['brand', 'model', 'year', 'price']);
            if($data) {
                $vehicle = Vehicle::find($id);
                if($vehicle) {
                    $vehicle->update($data);
                    return response()->json(['data'=> $vehicle], 200);
                } else {
                    return response()->json(['message'=>'O veículo com id '.$id.' não existe'], 400);
                }
            } else {
                    return response()->json(['message'=>'Dados inválidos'], 400);
            }
            
        }catch(\Exception $ex){
            return response()->json(['message'=>'erro ao se conectar com servidor'], 500);
        }        
    }

    public function destroy($id)
    {
        try{
            if($id < 0) {
                return response()->json(['message'=>'ID menor que zero, por favor, informe um ID válido'], 400);
            }
        
            $vehicle = Vehicle::find($id);
            if(!$vehicle) {
                return response()->json(['message'=>'O veículo com id '.$id.' não existe'], 404);
            }
            $exclude= $vehicle;
            $vehicle->delete();
            return response()->json(['message'=>'Foi excluido o registro'. $exclude .'com sucesso'], 204);
        
        }catch(\Exception $ex){
            return response()->json(['message'=>'erro ao se conectar com servidor'], 500);
        }
    }
}
