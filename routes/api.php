<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('usuario/add', 'User\UserController@register')->name('user.add');

Route::group(['middleware'=>['auth:api']], function(){
    Route::get('veiculos', 'Vehicle\VehicleController@index')->middleware('scope:admin,user');
    Route::post('veiculos/add', 'Vehicle\VehicleController@store')->middleware('scope:admin');
    Route::get('veiculos/{id}', 'Vehicle\VehicleController@findId')->middleware('scope:admin,user');
    Route::put('veiculos/alterar/{id}', 'Vehicle\VehicleController@update')->middleware('scope:admin');
    Route::delete('veiculos/excluir/{id}', 'Vehicle\VehicleController@destroy')->middleware('scope:admin');
});